import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import '../src/dist/css/zui.css'
import Head from './components/Head.vue';
import Register from './components/Register.vue';
import Footer from './components/Footer.vue';
import Login from './components/login.vue';
import Mylogin from './components/Mylogin.vue';
import Logout from './components/logout.vue';
import HomePage from './components/homePage.vue';
import List from './components/list.vue';
import Collection from './components/collection.vue';
import Commen from './components/commen.vue';
import Buy from './components/buy.vue';
import Findbea from './components/findbea.vue';
import Order from './components/order.vue';
import Order1 from './components/order1.vue';
import List1 from './components/list1.vue';
import Order2 from './components/order2.vue';
import Orderdetail from './components/orderdetail.vue';

import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);
import {
	Tab,
	Tabs
} from 'vant';
Vue.use(Tab);
Vue.use(Tabs);

import Router from 'vue-router'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
	return routerPush.call(this, location).catch(error => error)
}

import {
	Toast
} from 'mint-ui';
import Mint from 'mint-ui';
Vue.use(Mint);
import 'mint-ui/lib/style.css';
import axios from 'axios';
Vue.prototype.$axios = axios

Vue.filter('tab', function(value) {
	if (value == 0) {
		return 'vip';
	}
})
Vue.directive('rainbow', { //全局自定义指令,使用时用v-
	bind(el) { //bind：绑定用在哪个对象上
		el.style.color = "#" + Math.random().toString(16).slice(2, 8);
	}
})

Vue.use(VueRouter)
Vue.config.productionTip = false
//配置路由规则
var router = new VueRouter({
	mode: 'history', //可以去#
	routes: [{
			name: 'order',
			path: '/order',
			component: Order
		},
		{
			name: 'list1',
			path: '/list1',
			component: List1
		},
		{
			name: 'order2',
			path: '/order2',
			component: Order2
		},
		{
			name: 'orderdetail',
			path: '/orderdetail',
			component: Orderdetail
		},
		{
			name: 'order1',
			path: '/order1',
			component: Order1
		},
		{
			name: 'commen',
			path: '/commen',
			component: Commen
		},
		{
			name: 'findbea',
			path: '/findbea',
			components: {
				// header: Head,
				default: Findbea,
				footer: Footer
			}
		},
		{
			name: 'login',
			path: '/login',
			component: Login
		},
		{
			name: 'register',
			path: '/register',
			component: Register
		},
		{
			name: 'mylogin',
			path: '/mylogin',
			components: {
				// header: Head,
				default: Mylogin,
				footer: Footer
			}
		},
		{
			name: 'logout',
			path: '/logout',
			component: Logout
		},
		{
			name: 'homePage',
			path: '/',
			components: {
				header: Head,
				default: HomePage,
				footer: Footer
			}
		},
		{
			name: 'list',
			path: '/list',
			component: List
		},
		{
			name: 'collection',
			path: '/collection',
			component: Collection
		},
		{
			name: 'buy',
			path: '/buy',
			component: Buy
		}
	]
});
//拦截作用
router.beforeEach(function(to, from, next) {
	var loged_in = false;
	var getuserflag = JSON.parse(window.localStorage.getItem('key'))
	loged_in = getuserflag.logged;
	if (!loged_in && to.path == '/mylogin') {
		next('/login') //不满足条件到下一站/login
		Toast('您未登录，请先登录！')
	} else {
		next()
	}
})

new Vue({
	router: router,
	render: h => h(App), //渲染函数
}).$mount('#app')
